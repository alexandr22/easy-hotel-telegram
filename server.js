const heroku = require('./any/heroku'),
    TelegramBor = require('node-telegram-bot-api'),
    onMessage = require('./server/onMessage'),
    onCallback = require('./server/onCallback'),
    updatePool = require('./server/core/pool/update'),
    updateErrors = require('./server/core/errors/update'),
    cleaningStore = require('./server/reservation/placement/cleaningStore'),

    BOT_TOKEN = require('./any/isProdServer') ? "570757344:AAEGGIPp6JRsrmJgo-XgYjFbHTRHz01Abbo" : "523217964:AAHoJmVPZSzKTlocTcalBfAUfYF5FmmuR5o",

    timer = () => {
        setTimeout(timer, 1000);

        updatePool();
        updateErrors();
        cleaningStore();
    };

let bot =  new TelegramBor(BOT_TOKEN, require('./any/isProdServer') ?
    {polling: true} : {
        polling: true,
        request: {
            proxy: "http://185.18.255.85:3128"
        }
    }
);

bot.on('message', (msg) => {
    onMessage(msg, bot);
});

bot.on('callback_query', (msg) => {
    msg.message.text = msg.data;
    onCallback(msg.message, bot);
});

bot = heroku(bot);

timer();