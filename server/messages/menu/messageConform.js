const setMenu = require('../../core/user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    mainTypes = require('../../core/menu/types'),

    sendAll = require('../send/all'),
    sendPartners = require('../send/partners'),

    messageConform = require('../messages/messageConform'),
    selectivelyMessage = require('../messages/selectivelyMessage'),
    mainMenu = require('../../core/messages/mainMenu');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.SEND_EVERYBODY:
            sendAll(user, bot);

            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case STRINGS.SEND_PARTNERS:
            sendPartners(user, bot);

            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case STRINGS.SEND_SELECTIVELY:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                selectivelyMessage(msg, user, bot, () => {
                    mainMenu(msg, user, bot);
                });
            });
            break;

        case STRINGS.CANCEL:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            messageConform(msg, user, bot);
    }
};