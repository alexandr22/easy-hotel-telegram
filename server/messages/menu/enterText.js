const setMenu = require('../../core/user/setMenu'),
    STRINGS = require('../../core/local/rus'),
    checkLength = require('../../core/any/checkLength'),

    mainTypes = require('../../core/menu/types'),
    types = require('./types'),

    messageConform = require('../messages/messageConform'),
    mainMenu = require('../../core/messages/mainMenu');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.CANCEL:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            msg.text = checkLength(msg.text, 10000);
            user.temp = {text: msg.text};
            setMenu(types.MESSAGE_CONFORM, user.temp, user, () => {
                messageConform(msg, user, bot);
            });
    }
};