const {MESSAGE_CONFORM, ENTER_TEXT} = require('./menu/types'),
    messageConform = require('./menu/messageConform'),
    enterText = require('./menu/enterText');

module.exports = (msg, user, bot) => {
    switch (user.menu) {
        case MESSAGE_CONFORM:
            messageConform(msg, user, bot);
            break;

        case ENTER_TEXT:
            enterText(msg, user, bot);
            break;
    }
};