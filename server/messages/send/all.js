const STRINGS = require('../../core/local/rus'),
    {DATABASE_USER_GET_BY_CITY} = require('../../core/user/pSQL'),
    onError = require('../../core/errors'),
    query = require('../../core/pool');

module.exports = (user, bot) => {
    query(DATABASE_USER_GET_BY_CITY, [user.city, user.id], (error, data) => {
        for (let key in data.rows) {
            bot.sendMessage(data.rows[key].id, STRINGS.HOTEL_MESSAGE + " " + user.name + " " + user.temp.text);
        }
        bot.sendMessage(user.id, STRINGS.MESSAGE_READY_ALL);
    });
};