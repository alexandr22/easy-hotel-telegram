const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors'),
    partnersList = require('../../pairs/pairing/partnersList');

module.exports = (user, bot) => {
    partnersList(user, (list) => {
        for (let key in list) {
            bot.sendMessage(list[key], STRINGS.HOTEL_MESSAGE + " " + user.name + " " + user.temp.text);
        }
        bot.sendMessage(user.id, STRINGS.MESSAGE_READY_PARTNERS);
    });
};