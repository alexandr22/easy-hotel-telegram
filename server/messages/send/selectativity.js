const STRINGS = require('../../core/local/rus'),
    getUserById = require('../../core/user/getUserById'),
    onError = require('../../core/errors');

module.exports = (id, user, bot) => {
    if (!user.temp.text) return;
    getUserById(id, (partner) => {
        try {
            bot.sendMessage(partner.id, STRINGS.HOTEL_MESSAGE + " " + user.name + " " + user.temp.text);
            bot.sendMessage(user.id, STRINGS.MESSAGE_READY_SELECTED + " " + partner.name);
        } catch (e) {onError(e, 255555, user, bot);}
    });
};