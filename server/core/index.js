const {MAIN, SET_NAME, START, PRE_START, SETTINGS, START_CONTACTS, SET_CONTACTS, HOTEL_SETTINGS, PARTNER_SETTINGS} = require('./menu/types'),
    main = require('./menu/main'),
    setName = require('./menu/setName'),
    preStart = require('./menu/preStart'),
    settings = require('./menu/settings'),
    partnersSettings = require('./menu/partnersSettings'),
    hotelSettings = require('./menu/hotelSettings'),
    startContacts = require('./menu/startContacts'),
    setContacts = require('./menu/setContacts'),
    start = require('./menu/start');

module.exports = (msg, user, bot) => {
    switch (user.menu) {
        case MAIN:
            main(msg, user, bot);
            break;

        case SETTINGS:
            settings(msg, user, bot);
            break;

        case HOTEL_SETTINGS:
            hotelSettings(msg, user, bot);
            break;

        case PARTNER_SETTINGS:
            partnersSettings(msg, user, bot);
            break;

        case SET_NAME:
            setName(msg, user, bot);
            break;

        case START:
            start(msg, user, bot);
            break;

        case START_CONTACTS:
            startContacts(msg, user, bot);
            break;

        case SET_CONTACTS:
            setContacts(msg, user, bot);
            break;

        case PRE_START:
            preStart(msg, user, bot);
            break;
    }
};