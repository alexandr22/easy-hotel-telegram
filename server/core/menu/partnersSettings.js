const setMenu = require('../user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    types = require('./types'),

    settings = require('../messages/settings'),
    mainMenu = require('../messages/mainMenu'),
    partnersSettings = require('../messages/partnersSettings'),
    hotelsList = require('../../pairs/messages/hotelsList'),
    pairsList = require('../../pairs/messages/pairsList');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.CREATE_PAIR:
            hotelsList(msg, user, bot);
            break;

        case STRINGS.BREAK_PAIR:
            pairsList(msg, user, bot);
            break;

        case STRINGS.BACK:
            setMenu(types.SETTINGS, user.temp, user, () => {
                settings(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(types.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            partnersSettings(msg, user, bot);
    }
};