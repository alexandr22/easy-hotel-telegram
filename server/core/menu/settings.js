const setMenu = require('../user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    types = require('./types'),

    mainMenu = require('../messages/mainMenu'),
    settings = require('../messages/settings'),
    hotelSettings = require('../messages/hotelSettings'),
    partnersSettings = require('../messages/partnersSettings');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.YOUR_PARTNERS:
            setMenu(types.PARTNER_SETTINGS, user.temp, user, () => {
                partnersSettings(msg, user, bot);
            });
            break;

        case STRINGS.HOTEL_SETTINGS:
            setMenu(types.HOTEL_SETTINGS, user.temp, user, () => {
                hotelSettings(msg, user, bot);
            });
            break;

        case STRINGS.BACK:
            setMenu(types.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(types.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            settings(msg, user, bot);
    }
};