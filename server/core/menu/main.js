const setMenu = require('../user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    reservationTypes = require('../../reservation/menu/types'),
    messagesTypes = require('../../messages/menu/types'),
    types = require('./types'),

    mainMenu = require('../messages/mainMenu'),
    settings = require('../messages/settings'),
    carma = require('../messages/carma'),
    enterText = require('../../messages/messages/enterText'),
    reservationText = require('../../reservation/messages/reservationText');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.PLACEMENT_RESERVATION:
            setMenu(reservationTypes.RESERVATION_TEXT, {}, user, () => {
                reservationText(msg, user, bot);
            });
            break;

        case STRINGS.SETTINGS:
            setMenu(types.SETTINGS, {}, user, () => {
                settings(msg, user, bot);
            });
            break;

        case STRINGS.CARMA:
            carma(msg, user, bot);
            break;

        case STRINGS.MESSAGE:
            console.log(user.id);
            setMenu(messagesTypes.ENTER_TEXT, {}, user, () => {
                enterText(msg, user, bot);
            });
            break;

        default:
            mainMenu(msg, user, bot);
    }
};