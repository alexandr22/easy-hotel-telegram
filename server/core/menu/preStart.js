const setMenu = require('../user/setMenu'),
    types = require('./types'),
    start = require('../messages/start');

module.exports = (msg, user, bot) => {
    setMenu(types.START, user.temp, user, () => {
        start(msg, user, bot);
    });
};