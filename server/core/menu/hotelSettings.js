const setMenu = require('../user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    types = require('./types'),

    changeCity = require('../../city/messages/changeCity'),
    settings = require('../messages/settings'),
    mainMenu = require('../messages/mainMenu'),
    hotelSettings = require('../messages/hotelSettings'),
    setContacts = require('../messages/setContacts'),
    setName = require('../messages/setName');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.HOTEL_CONTACTS:
            setMenu(types.SET_CONTACTS, user.temp, user, () => {
                setContacts(msg, user, bot);
            });
            break;

        case STRINGS.CITY:
            changeCity(msg, user, bot);
            break;

        case STRINGS.NAME:
            setMenu(types.SET_NAME, user.temp, user, () => {
                setName(msg, user, bot);
            });
            break;

        case STRINGS.BACK:
            setMenu(types.SETTINGS, user.temp, user, () => {
                settings(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(types.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            hotelSettings(msg, user, bot);
    }
};