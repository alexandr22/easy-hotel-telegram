const setMenu = require('../user/setMenu'),
    types = require('./types'),
    setContacts = require('../user/setContacts'),
    mainMenu = require('../messages/mainMenu'),
    checkLength = require('../any/checkLength'),
    changeCity = require('../../city/messages/changeCity');

module.exports = (msg, user, bot) => {
    msg.text = checkLength(msg.text, 255);
    setContacts(msg.text, user, () => {
        setMenu(types.MAIN, user.temp, user, () => {
            changeCity(msg, user, bot, () => {
                mainMenu(msg, user, bot);
            });
        });
    });
};