const setMenu = require('../user/setMenu'),
    types = require('./types'),
    setName = require('../user/setName'),
    checkLength = require('../any/checkLength'),
    startContacts = require('../messages/startContacts');

module.exports = (msg, user, bot) => {
    msg.text = checkLength(msg.text, 25);
    setName(msg.text, user, () => {
        setMenu(types.START_CONTACTS, user.temp, user, () => {
            startContacts(msg, user, bot);
        });
    });
};