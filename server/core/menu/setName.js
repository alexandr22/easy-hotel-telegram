const setMenu = require('../user/setMenu'),
    STRINGS = require('../../core/local/rus'),
    types = require('./types'),
    setName = require('../user/setName'),
    mainMenu = require('../messages/mainMenu'),
    checkLength = require('../any/checkLength'),
    hotelSettings = require('../messages/hotelSettings');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.CANCEL:
            setMenu(types.HOTEL_SETTINGS, user.temp, user, () => {
                hotelSettings(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(types.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            msg.text = checkLength(msg.text, 25);
            setName(msg.text, user, () => {
                setMenu(types.HOTEL_SETTINGS, user.temp, user, () => {
                    hotelSettings(msg, user, bot);
                });
            });
    }
};