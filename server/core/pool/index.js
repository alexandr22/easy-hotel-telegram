const pg = require('pg'),
    {DATABASE_URL} = require('./config'),
    onError = require('../../core/errors'),
    poolStore = require('./store');

module.exports = (query, data, callback) => {
    try {
        if (!poolStore.pool) {
            poolStore.pool = pg.Pool({connectionString: DATABASE_URL});
            console.log("Pool opened!");
        }

        poolStore.lastRequest = new Date().getTime();

        poolStore.pool.query(query, data, callback);
    } catch (e) {
        onError(e, 262);
    }
};