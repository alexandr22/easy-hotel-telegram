const {POOL_LIVE_TIME} = require('./config'),
    onError = require('../../core/errors'),
    poolStore = require('./store');

module.exports = () => {
    try {
        if (!!poolStore.pool && (new Date().getTime() - POOL_LIVE_TIME > poolStore.lastRequest)) {
            poolStore.pool.end();
            poolStore.pool = null;
            console.log("Pool closed!");
        }
    } catch (e) {
        onError(e, 264);
    }
};