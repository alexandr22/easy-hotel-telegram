const addUser = require('./addUser'),
    getUserById = require('./getUserById');

module.exports = (msg, callback) => {
    getUserById(msg.chat.id, (user) => {
        if (!user) {
            addUser(msg, (newUser) => {
                callback(newUser);
            });
        }
        else {
            callback(user);
        }
    });
};