const query = require('../pool'),
    onError = require('../../core/errors'),
    {DATABASE_USER_SET_NAME} = require('./pSQL');

module.exports = (name, user, callback) => {
    query(DATABASE_USER_SET_NAME, [
        name,
        user.id,
    ], (error) => {
        if (error) return onError(error, 2981, user);
        try {
            user.name = name;
            callback();
        } catch (e) {
            onError(e, 2982, user);
        }
    });
};