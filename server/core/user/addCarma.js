const query = require('../pool'),
    getUserById = require('./getUserById'),
    onError = require('../../core/errors'),
    {DATABASE_USER_SET_CARMA_IN, DATABASE_USER_SET_CARMA_OUT} = require('./pSQL');

module.exports = (user_id, is_in, callback) => {
    getUserById(user_id, (user) => {
        try {
            if (is_in) {
                query(DATABASE_USER_SET_CARMA_IN, [
                    ++user.carma_in,
                    user.id,
                ], (error) => {
                    if (error) return onError(error, 2911, user);
                    if (callback) callback();
                });
            }
            else {
                query(DATABASE_USER_SET_CARMA_OUT, [
                    ++user.carma_out,
                    user.id,
                ], (error) => {
                    if (error) return onError(error, 2912, user);
                    if (callback) callback();
                });
            }
        } catch (e) {
            onError(e, 291);
        }
    });
};