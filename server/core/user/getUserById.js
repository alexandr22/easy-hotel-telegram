const query = require('../pool'),
    onError = require('../../core/errors'),
    {DATABASE_USER_GET_BY_ID} = require('./pSQL');

module.exports = (id, callback) => {
    query(DATABASE_USER_GET_BY_ID, [id], (error, data) => {
        if (error) return onError(error, 293);
        try {
            callback(data.rows[0]);
        } catch (e) {
            onError(e, 2931);
        }
    });
};