const {DATABASE_USER_ADD} = require('./pSQL'),
    {PRE_START} = require('../menu/types'),
    query = require('../pool');

module.exports = (msg, callback) => {
    try {
        query(DATABASE_USER_ADD, [
            msg.chat.id,
            PRE_START,
            "",
            {},
            -1,
            0,
            0,
            "",
        ],(error) => {
            if (error) return onError(error, 2922);
            callback({
                id: msg.chat.id,
                menu: PRE_START,
                name: "",
                temp: {},
            });
        });
    } catch (error) {
        if (error) return onError(error, 2921);
    }
};