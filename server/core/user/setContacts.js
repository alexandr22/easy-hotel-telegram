const query = require('../pool'),
    onError = require('../../core/errors'),
    {DATABASE_USER_SET_CONTACTS} = require('./pSQL');

module.exports = (contacts, user, callback) => {
    query(DATABASE_USER_SET_CONTACTS, [
        contacts,
        user.id,
    ], (error) => {
        if (error) return onError(error, 2961, user);
        try {
            user.contacts = contacts;
            callback();
        } catch (e) {
            onError(e, 2962, user);
        }
    });
};