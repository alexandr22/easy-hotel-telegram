const query = require('../pool'),
    onError = require('../../core/errors'),
    {DATABASE_USER_SET_MENU} = require('./pSQL');

module.exports = (menu, temp, user, callback) => {
    query(DATABASE_USER_SET_MENU, [
        menu,
        temp,
        user.id,
    ], (error) => {
        if (error) return onError(error, 297, user);
        callback();
    });
};