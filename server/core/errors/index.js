const pg = require('pg'),
    {DATABASE_URL} = require('../pool/config'),
    message = require('./message'),
    adminMessage = require('./adminMessage'),
    store = require('./store');

module.exports = (error, code, user, bot) => {
    try {
        const pool = pg.Pool({connectionString: DATABASE_URL});

        pool.query("INSERT INTO errors(id, code, error) VALUES($1, $2, $3)", [
            new Date().getTime(),
            code,
            error.toString(),
        ], (error) => {console.log(error); pool.end();});
    } catch (e) {}

    store.push({code, error});

    message(user, bot);
    adminMessage(error, code, bot);
};