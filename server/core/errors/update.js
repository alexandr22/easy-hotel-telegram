const errors = require('./store'),
    printError = require('./printError');

let timer = 0;

module.exports = () => {
    timer++;

    if (timer > 60) {
        timer = 0;
        for (let key in errors) {
            printError(errors[key].code, errors[key].error);
        }
    }
};