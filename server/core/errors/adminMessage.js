const admins = require('../any/admins');

module.exports = (error, code, bot) => {
    try {
        if (!bot) return;

        for (let key in admins) {
            bot.sendMessage(admins[key], `Ошибка на сервере ${code}! ${error}`);
        }
    } catch (e) {}
};