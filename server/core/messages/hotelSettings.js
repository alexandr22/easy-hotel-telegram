const cityes = require('../../city/changeCity/citiesList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.HOTEL_CONTACTS, callback_data: '/deposit' }],
            [{ text: STRINGS.NAME, callback_data: '/deposit' }],
            [{ text: STRINGS.CITY, callback_data: '/deposit' }],
            [{ text: STRINGS.BACK, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.CONTACTS + ": " + user.contacts + STRINGS.NAME + ": " + user.name + STRINGS.CITY + ": " + cityes[user.city], MENU);
    } catch (e) {
        onError(e, 252, user, bot);
    }
};