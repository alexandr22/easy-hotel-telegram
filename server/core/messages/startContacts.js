const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.ENTER_CONTACTS);
    } catch (e) {
        onError(e, 259, user, bot);
    }
};