const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.ENTER_HOTEL_NAME);
    } catch (e) {
        onError(e, 258, user, bot);
    }
};