const createPartnersList = require('../../pairs/pairing/createPartnersList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.CREATE_PAIR, callback_data: '/deposit' }],
            [{ text: STRINGS.BREAK_PAIR, callback_data: '/deposit' }],
            [{ text: STRINGS.BACK, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    createPartnersList(user, (hotel) => hotel.name, (hotels) => {
        try {
            let list = "";
            for ( let key in hotels) {
                list += "• " + hotels[key] + ";\n";
            }
            bot.sendMessage(user.id, STRINGS.YOUR_PARTNERS + ":\n" + list, MENU);
        } catch (e) {
            onError(e, 254, user, bot);
        }
    });
};