const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.BACK, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.ENTER_NEW_NAME, MENU);
    } catch (e) {
        onError(e, 256, user, bot);
    }
};