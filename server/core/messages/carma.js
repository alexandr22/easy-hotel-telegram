const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.CARMA_OUT + ": " + user.carma_out + " ⭐️\n" + STRINGS.CARMA_IN + ": " + user.carma_in + " 🛎");
    } catch (e) {
        onError(e, 251, user, bot);
    }
};