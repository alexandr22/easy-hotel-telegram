const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.PLACEMENT_RESERVATION, callback_data: '/deposit' }],
            [{ text: STRINGS.MESSAGE, callback_data: '/deposit' }],
            [{ text: STRINGS.CARMA, callback_data: '/deposit' }],
            [{ text: STRINGS.SETTINGS, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.MAIN_MENU, MENU);
    } catch (e) {
        onError(e, 243, user, bot);
    }
};