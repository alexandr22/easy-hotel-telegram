const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.YOUR_PARTNERS, callback_data: '/deposit' }],
            [{ text: STRINGS.HOTEL_SETTINGS, callback_data: '/deposit' }],
            [{ text: STRINGS.BACK, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.SELECT_NEED_SETTINGS, MENU);
    } catch (e) {
        onError(e, 257, user, bot);
    }
};