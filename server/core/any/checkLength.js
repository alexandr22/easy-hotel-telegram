const onError = require('../../core/errors');

module.exports = (text, length) => {
    try {
        if (text.length > length) {
            return text.substring(0, length);
        }
        return text;
    } catch (e) {
        onError(e, 121);
    }
};