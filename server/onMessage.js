const user = require('./core/user'),
    core = require('./core'),
    messages = require('./messages'),
    reservation = require('./reservation');

module.exports = (msg, bot) => {
    user(msg, (user) => {
        core(msg, user, bot);
        messages(msg, user, bot);
        reservation(msg, user, bot);
    });
};