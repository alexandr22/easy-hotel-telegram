const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

const MENU = {
    reply_markup: JSON.stringify({
        keyboard: [
            [{ text: STRINGS.SEND_EVERYBODY, callback_data: '/deposit' }],
            [{ text: STRINGS.SEND_PARTNERS, callback_data: '/deposit' }],
            [{ text: STRINGS.SEND_SELECTIVELY, callback_data: '/deposit' }],
            [{ text: STRINGS.CANCEL, callback_data: '/deposit' }],
        ],
        resize_keyboard: true
    })
};

module.exports = (msg, user, bot) => {
    try {
        bot.sendMessage(user.id, STRINGS.SEE_YOUR_RESERVATION + ":")
            .then(() => {
                try {
                    bot.sendMessage(user.id, STRINGS.HOTEL_RESERVATION + " " + user.name + ":\n[" + user.temp.text + "]", MENU);
                } catch (e) {
                    onError(e, 4211, user, bot);
                }
            });
    } catch (e) {
        onError(e, 4212, user, bot);
    }
};