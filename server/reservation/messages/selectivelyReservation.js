const createPartnersList = require('../../pairs/pairing/createPartnersList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot, callback) => {
    const reservationId = new Date().getTime();

    createPartnersList(user, (hotel) => {
        return [{ text: hotel.carma_out + " ⭐️ " + hotel.carma_in + " 🛎 " + hotel.name, callback_data: `/${reservationId}%push@${hotel.id}`}];
    }, (inline_keyboard) => {
        try {
            const MENU = {
                reply_markup: JSON.stringify({
                    inline_keyboard,
                    resize_keyboard: true
                })
            };

            bot.sendMessage(user.id, STRINGS.PARTNERS_FOR_REQUEST + ":", MENU)
                .then(() => {
                    callback();
                });
        } catch (e) {
            onError(e, 424, user, bot);
        }
    });
};