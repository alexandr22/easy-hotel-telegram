const {RESERVATION_CONFORM, RESERVATION_TEXT, RESERVATION_CONTACTS} = require('./menu/types'),
    reservationConform = require('./menu/reservationConform'),
    reservationContacts = require('./menu/reservationContacts'),
    reservationText = require('./menu/reservationText');

module.exports = (msg, user, bot) => {
    switch (user.menu) {
        case RESERVATION_CONFORM:
            reservationConform(msg, user, bot);
            break;

        case RESERVATION_CONTACTS:
            reservationContacts(msg, user, bot);
            break;


        case RESERVATION_TEXT:
            reservationText(msg, user, bot);
            break;
    }
};