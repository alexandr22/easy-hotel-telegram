const {DATABASE_RESERVATIONS_GET_BY_ID_NOT_END} = require('./pSQL'),
    STRINGS = require('../../core/local/rus'),
    query = require('../../core/pool');

module.exports = (reservationId, user, bot) => {
    query(DATABASE_RESERVATIONS_GET_BY_ID_NOT_END, [reservationId], (error, data) => {
        if (error) return onError(error, 4345, user, bot);
        try {
            if (data.rows.length === 0) return;

            bot.sendMessage(data.rows[0].user_id, STRINGS.YOUR_RESERVATION_IS_REJECT + " " + user.name + " " + STRINGS.YOUR_RESERVATION_IS_REJECT_2);
            for (let key in data.rows[0].subscribers.messages) {
                if (+data.rows[0].subscribers.messages[key].chat === +user.id) {
                    bot.deleteMessage(user.id, data.rows[0].subscribers.messages[key].message);
                }
            }
        } catch (e) {
            onError(e, 4321, user, bot);
        }
    });
};