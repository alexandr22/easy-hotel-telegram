const {DATABASE_RESERVATIONS_GET_BY_ID_NOT_END, DATABASE_RESERVATIONS_SET_END} = require('./pSQL'),
    STRINGS = require('../../core/local/rus'),
    getUserById = require('../../core/user/getUserById'),
    addCarma = require('../../core/user/addCarma'),
    onError = require('../../core/errors'),
    store = require('./store'),
    query = require('../../core/pool');

module.exports = (reservationId, user, bot) => {
    let isFirst = true;
    for (let key in store.reservations) {
        if (store.reservations[key].id === reservationId)
            isFirst = false;
    }

    if (!isFirst) return;

    store.reservations.push({id: reservationId, time: new Date().getTime()});

    query(DATABASE_RESERVATIONS_GET_BY_ID_NOT_END, [reservationId], (error, data) => {
        if (error) return onError(error, 4325, user, bot);
        try {
            if (data.rows.length === 0) return;

            for (let keyM in data.rows[0].subscribers.messages) {
                bot.deleteMessage(data.rows[0].subscribers.messages[keyM].chat, data.rows[0].subscribers.messages[keyM].message);

                if (data.rows[0].subscribers.messages[keyM].chat === +user.id) {
                    getUserById(data.rows[0].user_id, (hostUser) => {
                        bot.sendMessage(user.id, STRINGS.YOU_TAKE_RESERVATION + " [" + data.rows[0].text + "]\n" + STRINGS.YOU_TAKE_RESERVATION_2 + " " + hostUser.name + "\n" + hostUser.contacts + "\n" + STRINGS.CLIENT_CONTACTS + ": " + data.rows[0].contacts)
                    });

                    addCarma(user.id, true);
                }
            }

            bot.sendMessage(data.rows[0].user_id, STRINGS.YOUR_RESERVATION_IS_TAKE + " [" + data.rows[0].text + "] \n" + STRINGS.YOUR_RESERVATION_IS_TAKE_2 + " " + user.name + "\n" + user.contacts);

            addCarma(data.rows[0].user_id, false);

            query(DATABASE_RESERVATIONS_SET_END, [data.rows[0].id], () => {if (error) return onError(error, 4322, user, bot);});
        } catch (e) {
            onError(e, 4321, user, bot);
        }
    });
};