const store = require('./store');

let timer = 0;

module.exports = () => {
    timer++;

    if (timer > 100) {
        const newStore = [];
        const date = new Date().getTime();
        for (let key in store.reservations) {
            if (store.reservations[key].time < date - 10000) {
                newStore.push(store.reservations[key]);
            }
        }
        store.reservations = newStore;
    }
};