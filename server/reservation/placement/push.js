const {DATABASE_RESERVATIONS_ADD, DATABASE_RESERVATIONS_SET_SUBSCRIBERS, DATABASE_RESERVATIONS_GET_BY_ID} = require('./pSQL'),
    STRINGS = require('../../core/local/rus'),
    {DATABASE_USER_GET_BY_CITY} = require('../../core/user/pSQL'),
    onError = require('../../core/errors'),
    getUserById = require('../../core/user/getUserById'),
    random = require('../../core/any/random'),
    query = require('../../core/pool');

const send = (user, bot, list, reservationOld, reservationId = (new Date().getTime() + random()), target) => {
    try {
        const MENU = {
            reply_markup: JSON.stringify({
                inline_keyboard: [
                    [{ text: STRINGS.TAKE, callback_data: '/pull@' + reservationId}],
                    [{ text: STRINGS.REJECT, callback_data: '/reject@' + reservationId}],
                ],
                resize_keyboard: true
            })
        };

        const messages = [];

        for (let key in list) {
            bot.sendMessage(list[key], STRINGS.HOTEL_RESERVATION + " " +user.name + ":\n[" + user.temp.text + "]", MENU)
                .then((message) => {
                    try {
                        messages.push({message: message.message_id, chat: list[key]});

                        if (messages.length === list.length) {
                            if (reservationOld) {
                                reservationOld.subscribers.messages = [...reservationOld.subscribers.messages, ...messages];
                                query(DATABASE_RESERVATIONS_SET_SUBSCRIBERS, [
                                    reservationOld.subscribers,
                                    reservationOld.id,
                                ], (error) => {if (error) return onError(error, 4339, user, bot);});
                            }
                            else {
                                query(DATABASE_RESERVATIONS_ADD, [
                                    reservationId,
                                    STRINGS.HOTEL_RESERVATION + " " + user.name + ":\n[" + user.temp.text + "]",
                                    user.temp.contacts,
                                    {messages},
                                    +user.id,
                                    false,
                                ], (error) => {if (error) return onError(error, 4338, user, bot);});
                            }
                        }
                    } catch (e) {
                        onError(e, 4337, user, bot);
                    }
                });
        }

        switch (target) {
            case 0:
                bot.sendMessage(user.id, STRINGS.YOU_PUSH_RESERVATION_ALL);
                break;

            case 1:
                bot.sendMessage(user.id, STRINGS.YOU_PUSH_RESERVATION_PARTNERS);
                break;

            case 2:
                getUserById(list[0], (partner) => {
                    bot.sendMessage(user.id, STRINGS.YOU_PUSH_RESERVATION_SELECTIVITY + " " + partner.name);
                });
                break;
        }
    } catch (e) {
        onError(e, 4336, user, bot);
    }
};

const checkSecond = (user, bot, list, reservationId, target) => {
    if (reservationId) {
        query(DATABASE_RESERVATIONS_GET_BY_ID, [reservationId], (error, data) => {
            if (error) return onError(error, 4334, user, bot);
            try {
                if (data.rows.length === 0) {
                    return send(user, bot, list, null, reservationId, target);
                }

                if (data.rows[0].is_end) return;

                let isFirst = true;
                for (let key2 in data.rows[0].subscribers.messages) {
                    if (data.rows[0].subscribers.messages[key2].chat === list[0])
                        isFirst = false;
                }

                if (isFirst)
                    send(user, bot, list, data.rows[0], reservationId, target);
            } catch (e) {
                onError(e, 4333, user, bot);
            }
        });
    }
    else {
        send(user, bot, list, null, reservationId, target);
    }
};

module.exports = (text, user, bot, list, reservationId, target) => {
    try {
        if (!user.temp.text) return;

        if (list) return checkSecond(user, bot, list, reservationId, target);

        query(DATABASE_USER_GET_BY_CITY, [user.city, user.id], (error, data) => {
            if (error) return onError(error, 4335, user, bot);
            try {
                const usersList = [];

                for (let key in data.rows) {
                    usersList.push(+data.rows[key].id);
                }

                checkSecond(user, bot, usersList, reservationId, target);
            } catch (e) {
                onError(e, 4332, user, bot);
            }
        });
    } catch (e) {
        onError(e, 4331, user, bot);
    }
};