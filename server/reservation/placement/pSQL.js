const DATABASE_RESERVATIONS_ADD = "INSERT INTO reservations(id, text, contacts, subscribers, user_id, is_end) VALUES($1, $2, $3, $4, $5, $6)",
    DATABASE_RESERVATIONS_GET = "SELECT * FROM reservations",
    DATABASE_RESERVATIONS_GET_BY_ID = "SELECT * FROM reservations WHERE id = $1",
    DATABASE_RESERVATIONS_GET_BY_ID_NOT_END = "SELECT * FROM reservations WHERE id = $1 AND is_end = false",
    DATABASE_RESERVATIONS_SET_SUBSCRIBERS = "UPDATE reservations SET subscribers=$1 WHERE id = $2",
    DATABASE_RESERVATIONS_SET_END = "UPDATE reservations SET is_end=true WHERE id = $1";

module.exports = {DATABASE_RESERVATIONS_ADD, DATABASE_RESERVATIONS_GET, DATABASE_RESERVATIONS_SET_END, DATABASE_RESERVATIONS_SET_SUBSCRIBERS, DATABASE_RESERVATIONS_GET_BY_ID_NOT_END, DATABASE_RESERVATIONS_GET_BY_ID};