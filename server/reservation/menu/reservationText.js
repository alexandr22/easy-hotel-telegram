const setMenu = require('../../core/user/setMenu'),
    STRINGS = require('../../core/local/rus'),
    checkLength = require('../../core/any/checkLength'),

    mainTypes = require('../../core/menu/types'),
    types = require('./types'),

    reservationContacts = require('../messages/reservationContacts'),
    mainMenu = require('../../core/messages/mainMenu');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.CANCEL:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            msg.text = checkLength(msg.text, 1023);
            user.temp = {text: msg.text};
            setMenu(types.RESERVATION_CONTACTS, user.temp, user, () => {
                reservationContacts(msg, user, bot);
            });
    }
};