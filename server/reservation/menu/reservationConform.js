const setMenu = require('../../core/user/setMenu'),
    STRINGS = require('../../core/local/rus'),

    mainTypes = require('../../core/menu/types'),

    pushReservation = require('../placement/push'),
    partnersList = require('../../pairs/pairing/partnersList'),

    reservationConform = require('../messages/reservationConform'),
    selectivelyReservation = require('../messages/selectivelyReservation'),
    mainMenu = require('../../core/messages/mainMenu');

module.exports = (msg, user, bot) => {
    switch (msg.text) {
        case STRINGS.SEND_EVERYBODY:
            pushReservation(user.temp.text, user, bot, undefined, undefined, 0);

            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case STRINGS.SEND_PARTNERS:
            partnersList(user, (list) => {
                pushReservation(user.temp.text, user, bot, list, undefined, 1);
            });

            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case STRINGS.SEND_SELECTIVELY:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                selectivelyReservation(msg, user, bot, () => {
                    mainMenu(msg, user, bot);
                });
            });
            break;

        case STRINGS.CANCEL:
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        case "/main_menu":
            setMenu(mainTypes.MAIN, user.temp, user, () => {
                mainMenu(msg, user, bot);
            });
            break;

        default:
            reservationConform(msg, user, bot);
    }
};