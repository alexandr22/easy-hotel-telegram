const DATABASE_PAIRS_GET = "SELECT * FROM pairs",
    DATABASE_PAIRS_GET_BY_USER = "SELECT * FROM pairs WHERE (lead_id = $1 OR flown_id = $1) AND is_conform = true",
    DATABASE_PAIRS_GET_BY_USERS = "SELECT * FROM pairs WHERE ((lead_id = $1 AND flown_id = $2) OR (lead_id = $2 AND flown_id = $1)) AND is_conform = true",
    DATABASE_PAIRS_GET_BY_USERS_NOT_CONFORM = "SELECT * FROM pairs WHERE ((lead_id = $1 AND flown_id = $2) OR (lead_id = $2 AND flown_id = $1)) AND is_conform = false",
    DATABASE_PAIRS_GET_BY_USERS_ALL = "SELECT * FROM pairs WHERE (lead_id = $1 AND flown_id = $2) OR (lead_id = $2 AND flown_id = $1)",
    DATABASE_PAIRS_CONFORM = "UPDATE pairs SET is_conform=true WHERE id = $1",
    DATABASE_PAIRS_DELETE = "DELETE FROM pairs WHERE id = $1",
    DATABASE_PAIRS_ADD = "INSERT INTO pairs(id, lead_id, flown_id, is_conform) VALUES($1, $2, $3, $4)";

module.exports = {DATABASE_PAIRS_GET, DATABASE_PAIRS_ADD, DATABASE_PAIRS_CONFORM, DATABASE_PAIRS_DELETE, DATABASE_PAIRS_GET_BY_USER, DATABASE_PAIRS_GET_BY_USERS, DATABASE_PAIRS_GET_BY_USERS_NOT_CONFORM, DATABASE_PAIRS_GET_BY_USERS_ALL};