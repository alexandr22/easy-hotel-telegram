const {DATABASE_PAIRS_GET_BY_USERS, DATABASE_PAIRS_DELETE} = require('./pSQL'),
    pairBreak = require('../messages/break'),
    onError = require('../../core/errors'),
    query = require('../../core/pool');

module.exports = (flown_id, user, bot) => {
    query(DATABASE_PAIRS_GET_BY_USERS, [flown_id, user.id], (error, data) => {
        if (error) return onError(error, 3211, user, bot);

        try {
            if (data.rows.length === 0) return;

            query(DATABASE_PAIRS_DELETE, [
                data.rows[0].id,
            ], (error) => {
                if (error) return onError(error, 3212, user, bot);
                pairBreak(null, user, bot, flown_id);
            });
        } catch (e) {
            onError(e, 3213, user, bot);
        }

    });
};