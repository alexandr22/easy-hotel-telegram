const {DATABASE_PAIRS_GET_BY_USER} = require('./pSQL'),
    onError = require('../../core/errors'),
    query = require('../../core/pool');

module.exports = (user, callback) => {
    query(DATABASE_PAIRS_GET_BY_USER, [user.id], (error, dataPairs) => {
        if (error) return onError(error, 3242, user);
        try {
            const partners = [];

            for (let key in dataPairs.rows) {
                if (+dataPairs.rows[key].flown_id === +user.id) {
                    partners.push(+dataPairs.rows[key].lead_id);
                }
                else {
                    if (+dataPairs.rows[key].lead_id === +user.id) {
                        partners.push(+dataPairs.rows[key].flown_id);
                    }
                }
            }

            callback(partners);
        } catch (e) {
            onError(e, 3241, user);
        }
    });
};