const {DATABASE_PAIRS_GET_BY_USERS_ALL, DATABASE_PAIRS_ADD} = require('./pSQL'),
    pairConform = require('../messages/pairConform'),
    onError = require('../../core/errors'),
    random = require('../../core/any/random'),
    query = require('../../core/pool');

module.exports = (flown_id, user, bot) => {
    query(DATABASE_PAIRS_GET_BY_USERS_ALL, [flown_id, user.id], (error, data) => {
        if (error) return onError(error, 3261, user, bot);
        try {
            if (data.rows.length === 0) {
                query(DATABASE_PAIRS_ADD, [
                    new Date().getTime() + random(),
                    user.id,
                    flown_id,
                    false
                ], (error) => {
                    if (error) return onError(error, 3263, user, bot);
                    pairConform(null, user, bot, flown_id);
                });
            }
            else {
                if (!data.rows[0].is_conform) {
                    pairConform(null, user, bot, flown_id);
                }
            }
        } catch (e) {
            onError(e, 3262, user, bot);
        }
    });
};