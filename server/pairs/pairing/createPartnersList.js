const {DATABASE_USER_GET_BY_CITY} = require('../../core/user/pSQL'),
    partnersList = require('../pairing/partnersList'),
    onError = require('../../core/errors'),
    query = require('../../core/pool');

module.exports = (user, builder, callback) => {
    partnersList(user, (partners) => {
        query(DATABASE_USER_GET_BY_CITY, [user.city, user.id], (error, dataUsers) => {
            if (error) return onError(error, 3231, user);
            try {
                const array = [];

                for (let key in dataUsers.rows) {
                    for (let keyP in partners) {
                        if (+dataUsers.rows[key].id === partners[keyP]) {
                            array.push(builder(dataUsers.rows[key]));
                        }
                    }
                }

                callback(array);
            } catch (e) {
                onError(e, 3232, user);
            }
        });
    });
};