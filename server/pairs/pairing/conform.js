const {DATABASE_PAIRS_GET_BY_USERS_NOT_CONFORM, DATABASE_PAIRS_CONFORM} = require('./pSQL'),
    pairSuccess = require('../messages/pairSuccess'),
    onError = require('../../core/errors'),
    query = require('../../core/pool');

module.exports = (lead_id, user, bot) => {
    query(DATABASE_PAIRS_GET_BY_USERS_NOT_CONFORM, [lead_id, user.id], (error, data) => {
        if (error) return onError(error, 3223, user, bot);
        try {
            if (data.rows.length === 0) return;

            query(DATABASE_PAIRS_CONFORM, [
                data.rows[0].id,
            ], (error) => {
                if (error) return onError(error, 3222, user, bot);
                pairSuccess(null, user, bot, lead_id);
            });
        } catch (e) {
            onError(e, 3221, user, bot);
        }
    });
};