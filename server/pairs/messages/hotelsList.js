const {DATABASE_USER_GET_BY_CITY} = require('../../core/user/pSQL'),
    {DATABASE_PAIRS_GET_BY_USER} = require('../pairing/pSQL'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus'),
    query = require('../../core/pool');

const sendList = (arrays, i, user, bot) => {
    if (i >= arrays.length) return;

    const MENU = {
        reply_markup: JSON.stringify({
            inline_keyboard: arrays[i],
            resize_keyboard: true
        })
    };

    bot.sendMessage(user.id, STRINGS.HOTELS_IN_YOUR_CITY + ":", MENU)
        .then(() => {
            sendList(arrays, ++i, user, bot);
        });
};

module.exports = (msg, user, bot) => {
    query(DATABASE_USER_GET_BY_CITY, [user.city, user.id], (error, dataUsers) => {
        if (error) return onError(error, 3123, user, bot);
        query(DATABASE_PAIRS_GET_BY_USER, [user.id], (error, dataPairs) => {
            if (error) return onError(error, 3122, user, bot);
            try {
                const arrays = [[]];
                let i = 0;
                for (let keyUsers in dataUsers.rows) {
                    let isNotPartner = true;

                    for (let keyPairs in dataPairs.rows) {
                        if (+dataPairs.rows[keyPairs].lead_id === +dataUsers.rows[keyUsers].id || +dataPairs.rows[keyPairs].flown_id === +dataUsers.rows[keyUsers].id)
                            isNotPartner = false;
                    }

                    if (isNotPartner) {
                        if (arrays[i].length >= 30) {
                            i++;
                            arrays.push([]);
                        }

                        arrays[i].push([{ text: dataUsers.rows[keyUsers].carma_out + " ⭐️ " + dataUsers.rows[keyUsers].carma_in + " 🛎 " + dataUsers.rows[keyUsers].name, callback_data: '/pair@' + dataUsers.rows[keyUsers].id }]);
                    }
                }

                sendList(arrays, 0, user, bot);
            } catch (e) {
                onError(e, 3121, user, bot);
            }
        });
    });
};