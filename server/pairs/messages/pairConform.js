const STRINGS = require('../../core/local/rus'),
    onError = require('../../core/errors');

module.exports = (msg, user, bot, hotel_id) => {
    try {
        const MENU = {
            reply_markup: JSON.stringify({
                inline_keyboard: [
                    [{ text: STRINGS.PAIRING, callback_data: '/conform@' + user.id }],
                    [{ text: STRINGS.ON_PAIRING, callback_data: '/no_con@' + user.id }],
                ],
                resize_keyboard: true
            })
        };

        bot.sendMessage(hotel_id, STRINGS.HOTEL + " " + user.name + " " + STRINGS.HOTEL_CREATE_PAIR, MENU);

        bot.sendMessage(user.id, STRINGS.PAIR_REQUEST_SEND);
    } catch (e) {
        onError(e, 331, user, bot);
    }
};