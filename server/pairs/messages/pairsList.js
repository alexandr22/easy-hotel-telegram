const createPartnersList = require('../pairing/createPartnersList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot) => {
    createPartnersList(user, (hotel) => {
        return [{ text: hotel.carma_out + " ⭐️ " + hotel.carma_in + " 🛎 " + hotel.name, callback_data: '/break@' + hotel.id}];
    }, (inline_keyboard) => {
        try {
            const MENU = {
                reply_markup: JSON.stringify({
                    inline_keyboard,
                    resize_keyboard: true
                })
            };

            bot.sendMessage(user.id, STRINGS.HOTELS_FOR_BREAK_PAIR + ":", MENU);
        } catch (e) {
            onError(e, 314, user, bot);
        }
    });
};