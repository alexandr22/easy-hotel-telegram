const getUserById = require('../../core/user/getUserById'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot, hotel_id) => {
    getUserById(hotel_id, (hotel) => {
        try {
            bot.sendMessage(user.id, STRINGS.YOR_PAIRED_WITH + " " + hotel.name);

            bot.sendMessage(hotel_id, STRINGS.PAIR_REQUEST_WITH + "\n" + user.name + " " + STRINGS.CONFORMED);
        } catch (e) {
            onError(e, 315, user, bot);
        }
    });
};