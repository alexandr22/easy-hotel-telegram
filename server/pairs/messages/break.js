const getUserById = require('../../core/user/getUserById'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot, hotel_id) => {
    getUserById(hotel_id, (partner) => {
        try {
            bot.sendMessage(user.id, STRINGS.YOUR_BREAK_PAIR + " " + partner.name);

            bot.sendMessage(hotel_id, STRINGS.HOTEL + " " + user.name + " " + STRINGS.HOTEL_BREAK_PAIR);
        } catch (e) {
            onError(e, 311, user, bot);
        }
    });
};