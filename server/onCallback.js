const userCheeck = require('./core/user'),
    changeCity = require('./city/changeCity'),
    pairingRequest = require('./pairs/pairing/request'),
    pairingConform = require('./pairs/pairing/conform'),
    noConform = require('./pairs/pairing/noConform'),
    pushReservation = require('./reservation/placement/push'),
    rejectReservation = require('./reservation/placement/reject'),
    selectativityMessage = require('./messages/send/selectativity'),
    pairingBreak = require('./pairs/pairing/break'),
    pull = require('./reservation/placement/pull');

module.exports = (msg, bot) => {
    userCheeck(msg, (user) => {
        if (msg.text.indexOf("pull", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            pull(id, user, bot);
            return;
        }

        if (msg.text.indexOf("push", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            const id2 = +msg.text.substring(1, msg.text.indexOf("%", 0));
            pushReservation(null, user, bot, [id], id2, 2);
            return;
        }

        if (msg.text.indexOf("city", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            changeCity(id, user, bot);
            return;
        }

        if (msg.text.indexOf("pair", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            pairingRequest(id, user, bot);
            return;
        }

        if (msg.text.indexOf("conform", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            pairingConform(id, user, bot);
            return;
        }

        if (msg.text.indexOf("no_con", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            noConform(id, user, bot);
            return;
        }

        if (msg.text.indexOf("break", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            pairingBreak(id, user, bot);
            return;
        }

        if (msg.text.indexOf("reject", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            rejectReservation(id, user, bot);
            return;
        }

        if (msg.text.indexOf("message", 0) !== -1) {
            const id = +msg.text.substring(msg.text.indexOf("@", 0) + 1, msg.text.length);
            selectativityMessage(id, user, bot);
            return;
        }
    });
};