const cites = require('../changeCity/citiesList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot, callback) => {
    try {
        const inline_keyboard = [];
        for (let key in cites) {
            inline_keyboard.push([{ text: cites[key], callback_data: '/city@' + key}]);
        }

        const MENU = {
            reply_markup: JSON.stringify({
                inline_keyboard,
                resize_keyboard: true
            })
        };

        bot.sendMessage(user.id, STRINGS.SELECT_CITY, MENU)
            .then(() => {
                if (callback) callback();
            });
    } catch (e) {
        onError(e, 121, user, bot);
    }
};