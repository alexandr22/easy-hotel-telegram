const cites = require('../changeCity/citiesList'),
    onError = require('../../core/errors'),
    STRINGS = require('../../core/local/rus');

module.exports = (msg, user, bot, city) => {
    try {
        bot.sendMessage(user.id, STRINGS.YOU_CHANGE_CITY + " " + cites[city]);
    } catch (e) {
        onError(e, 122, user, bot);
    }
};