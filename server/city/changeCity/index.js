const query = require('../../core/pool'),
    onError = require('../../core/errors'),
    changeSuccess = require('../messages/changeSuccess'),
    {DATABASE_USERS_SET_CITY} = require('./pSQL');

module.exports = (city, user, bot) => {
    query(DATABASE_USERS_SET_CITY, [
        city,
        user.id
    ], (error) => {
        if (error) return onError(error, 112, user, bot);
        changeSuccess(null, user, bot, city);
    });
};