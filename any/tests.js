module.exports = (bot, server) => {
    const socketDriver = require('socket.io'),
        onMessage = require('../server/onMessage'),
        onCallback = require('../server/onCallback'),
        io = socketDriver(server);

    let socket;

    io.on('connection', (s) => {
        socket = s;

        socket.on('message', (data) => {
            onMessage(data, bot);
        });

        socket.on('callback', (data) => {
            onCallback(data, bot);
        });
    });

    let originalBot = bot;

    bot = {};

    bot.sendMessage = (id, text, menu) => {
        if (id > 0) {
            return originalBot.sendMessage(id, text, menu);
        }
        else {
            if (socket) socket.emit('send', {id, text, menu});
            return {then: (f) => {f({message: new Date().getTime(), chat: id})}};
        }
    };

    bot.deleteMessage = (id, message) => {
        if (id > 0) {
            return originalBot.deleteMessage(id, message);
        }
        else {
            return {then: (f) => {f()}};
        }
    };

    return bot;
};