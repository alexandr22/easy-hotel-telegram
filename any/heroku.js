module.exports = (bot) => {
    const express = require('express'),
        tests = require('./tests'),
        app = express();

    app.set('port', (process.env.PORT) || 9000);

    app.get('/uptime', (req, res) => {
        console.log(new Date().getTime());
        res.end('ok');
    });

    const server = app.listen(app.get('port'), () => console.log('Server is starting on port ' + app.get('port')));

    return tests(bot, server);
};